# How to Publish NPM Package


First, create a user account (if you don't already have one), and log on.

    npm login


(Don't forget to verify your email if you have just created a new account.)


Next, update your project's version (in `package.json`). Then, create a package tarball. 
For instance, from the project/package folder,

    npm pack


This creates a tgz flle with a name, `packagename-version.tgz` in PWD.
(This is optional, but I always do this to keep the snapshot of the release, if nothing else.) 


Now, you can publish the package to the NPM registry as follows,

    npm publish packagename-version.tgz --access public


The access flag can be one of "public" or "restricted". 
The default value, when you publich a package for the first time, is "public" for an individual package,
or it is "restriced" if you use NPM "Orgs". When you update/re-publish the package, 
this flag is not needed.


If you have successfully published your package, it'll show up in the following npm registry page:

* https://npmjs.com/package/your-package-name



_Note: as of this wriing, npm@5 has a bug in `npm publish`._ 
_You will get an error like this: "Cannot read property 'algorithm' of undefined" when you try to publish a tarball._ 
_The only workaround at this point seems to be downgrading your npm and use npm@4. I currently use npm@4.6.1._


## References


* [Publishing npm packages](https://docs.npmjs.com/getting-started/publishing-npm-packages)
* [npm-pack](https://docs.npmjs.com/cli/pack)
* [npm-publish](https://docs.npmjs.com/cli/publish)

